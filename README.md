#JiffCC

JiffCC is a mobile first, full stack web application allowing users to find company programs in the dashboard and navigate to the program's details to find out more.

## Client
----
>The client is built in Angular 1.5 with only two views and two controllers. The dashboard view displays all of the programs available to the user. The details view reveals the information for the program selected, including incentives and descriptions from the API. Navigation is handled through ui-router for smooth transitions while passing state. A single factory is handling requests to the server for both the dashboard and details controllers.


## Server
----
Efficiency Decision
>Initially the plan was to utilize ng-router to handle view transitions. However, this presented a problem with passing the selected program's state to the details controller.

Option 1
>One way of handling navigation would be to use ng-router with routeParams over to details view/controller. Then the details controller would need to make an additional request to the server in order to retrieve all of the details.

>Because of the dataset this would require another iteration over the program_blueprints dataset to find the correct id or name. Then of course once more over the goals dataset if that program contains goals.

Option 2
>Alternatively, ui-router could be utilized in order to pass a state object with stateParams. Since we already have accessed the program_blueprints dataset it would more efficient to send over the selected program when navigating.

Ultimately Option 2, using ui-router, was chosen for efficiency
