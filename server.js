var express = require('express');
var app = express();
var utils = require('./utils.js')
var bodyParser = require('body-parser');
var port = process.env.PORT || 3030;

app.use(express.static('client'));
app.use(bodyParser.json());

app.get('/api/programs', utils.fetchPrograms);
app.post('/api/goals', utils.fetchGoals);

app.listen(port, function () {
  console.log('Jiff app listening on port 3030!');
});
