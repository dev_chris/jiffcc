App.controller('detailsController', ['$scope','$stateParams', '$state', 'ProgramFactory', function($scope, $stateParams, $state, ProgramFactory) {
  //if the state is passed from dashboard
  if($stateParams.programParam) {
    $scope.program = $stateParams.programParam;
    $scope.header = $scope.program.title;
    $scope.goals = [];
    $scope.total = 0;

    //retrieves goals and updates $scope goals and total
    var intitializeGoals = function(goals) {
      return goals.map(function(goal) {
        return ProgramFactory.getGoals(goal)
        .then(function(results) {
          $scope.goals.push(results.data);
          $scope.total = $scope.total + results.data.incentive_value
        })
        .catch(function(err) {
          console.log(err);
        });
      });
    };

    //if program has goals then retrieve
    if($scope.program.goals) intitializeGoals($scope.program.goals);

  //redirect to dashboard if details does not recieve state
  } else {
    $state.go('dashboard');
  }
}]);
