App.factory('ProgramFactory', function($http) {
  var getPrograms = function () {
    return $http({
      method: 'GET',
      url: '/api/programs'
    })
    .then(function (response) {
      return response.data;
    });
  };
  var getGoals = function(goal) {
    return $http.post('/api/goals', goal)
    .success(function (data) {
      return data;
    }).error(function(err) { console.error(err) });
  };
  return {
    getPrograms: getPrograms,
    getGoals: getGoals
  };
});
