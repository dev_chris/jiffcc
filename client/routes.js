// ROUTES
App.config(function($stateProvider, $urlRouterProvider) {
  $urlRouterProvider.otherwise('/dashboard');

  $stateProvider
  .state('dashboard', {
    url: '/dashboard',
    templateUrl: 'dashboard/dashboard.html',
    controller: 'dashboardController'
  })
  .state('details', {
    url: '/details',
    params: {
      programParam: null
    },
    templateUrl: 'details/details.html',
    controller: 'detailsController'
  });
});
