App.controller('dashboardController', ['$scope', '$state', '$stateParams', 'ProgramFactory', function($scope, $state, $stateParams, ProgramFactory) {
  $scope.data = {};
  $scope.header = 'Dashboard';
  //gets all programs available for dashboard
  var initializePrograms = function () {
    ProgramFactory.getPrograms()
    .then(function (programs) {
      $scope.data.programs = programs.program_blueprints;
    })
    .catch(function (err) {
      console.error(err);
    });
  };
  initializePrograms();
}]);
