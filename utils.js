var programData = require('./data/program_blueprints.json');
var goalData = require('./data/goals.json');

module.exports = {
  fetchPrograms: function(req, res) {
    res.send(programData);
  },
  fetchGoals: function(req, res) {
    var goalId = req.body.guid;
    var data;
    goalData.goals.forEach(function(goal) {
      if(goalId === goal.guid) data = goal;
    });
    if(data) res.send(data);
  }
};
